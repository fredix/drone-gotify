FROM debian:stable
MAINTAINER fredix@protonmail.com

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates && \
    apt-get clean

ADD drone-gotify /bin/
ENTRYPOINT /bin/drone-gotify

