# drone-gotify

## documentations

https://gotify.net/docs/more-pushmsg#golang  
https://docs.drone.io/plugins/golang/

## environment variables :
       
```
PLUGIN_GOTIFY_ENDPOINT="https://gotify.server.tld"
PLUGIN_GOTIFY_TOKEN="GOTIFY_APPLICATION_TOKEN"
PLUGIN_GOTIFY_TITLE="from drone-gotify"
PLUGIN_MESSAGE="test"          
PLUGIN_GOTIFY_PRIORITY="5"     
```
Example in a .drone.yml

```
- name: gotify
  image: fredix/arm64v8-alpine-drone-gotify
  settings:
    gotifytoken:
      from_secret: plugin_gotifytoken
    gotifyendpoint:
      from_secret: plugin_gotifyendpoint
    gotifytitle:
      from_secret: plugin_gotifytitle
    gotifypriority:
      from_secret: plugin_gotifypriority
    message: >
      {{#success build.status}}
        build {{build.number}} succeeded on {{repo.name}}. Good job {{build.author}}  {{build.link}}
      {{else}}
        build {{build.number}} failed on {{repo.name}}. Fix me please {{build.author}}  {{build.link}}
      {{/success}}
  when:
    status: [ success, failure ]
```
