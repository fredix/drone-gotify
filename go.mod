module framagit.org/fredix/drone-gotify

go 1.14

require (
	github.com/drone/drone-template-lib v1.0.0
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.5.0
	github.com/urfave/cli v1.22.4
)
